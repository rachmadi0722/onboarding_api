﻿namespace onboarding_API.Model
{
    public class ResponseViewModel<T>
    {
        public int StatusCode { get; set; }
        public string? Message { get; set; }
        public T? Data { get; set; }
    }
}
