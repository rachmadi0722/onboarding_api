﻿using System.ComponentModel.DataAnnotations;

namespace onboarding_API.Model
{
    public class Member
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(250)]
        public string Address { get; set; }
    }
}
