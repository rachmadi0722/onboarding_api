﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using onboarding_API.Data;
using onboarding_API.Helper;
using onboarding_API.Model;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace onboarding_API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class MemberController : ControllerBase
    {
        private readonly ApplicationDbContext _db;

        public MemberController(ApplicationDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        public ResponseViewModel<List<Member>> GetAll()
        {
            List<Member> members = _db.members.ToList();
            return new ResponseViewModel<List<Member>>()
            {
                StatusCode = 200,
                Message = "Success Get Data",
                Data = members
            };
        }

        [HttpGet("{id}"), Authorize]
        public ResponseViewModel<List<Member>> GetById(int id)
        {
            List<Member> members = _db.members.Where(s => s.Id == id).ToList();

            if (members == null)
            {
                return new ResponseViewModel<List<Member>>()
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }

            return new ResponseViewModel<List<Member>>()
            {
                StatusCode = 200,
                Message = "Success Get Data",
                Data = members
            };
        }

        [HttpPost]
        public ResponseViewModel<List<Member>> Post(Member obj)
        {
            if (ModelState.IsValid)
            {
                _db.members.Add(obj);
                _db.SaveChanges();
                List<Member> data = new List<Member>();
                data.Add(obj);
                return new ResponseViewModel<List<Member>>()
                {
                    StatusCode = 200,
                    Message = "Success Insert Data",
                    Data = data
                };
            }
            return new ResponseViewModel<List<Member>>()
            {
                StatusCode = 400,
                Message = "Invalid Data",
                Data = null
            };
        }

        [HttpPut("{id}")]
        public ResponseViewModel<List<Member>> Put(int id, Member obj)
        {
            obj.Id = id;
            if(ModelState.IsValid)
            {
                _db.members.Update(obj);
                _db.SaveChanges();

                List<Member> data = new List<Member>();
                data.Add(obj);
                return new ResponseViewModel<List<Member>>()
                {
                    StatusCode = 200,
                    Message = "Success Update Data",
                    Data = data
                };
            }
            return new ResponseViewModel<List<Member>>()
            {
                StatusCode = 400,
                Message = "Invalid Data",
                Data = null
            };
        }

        [HttpDelete("{id}")]
        public ResponseViewModel<List<Member>> Delete(int id)
        {
            var obj = _db.members.Find(id);
            if (obj == null)
            {
                return new ResponseViewModel<List<Member>>()
                {
                    StatusCode = 404,
                    Message = "Data Not Found",
                    Data = null
                };
            }

            _db.members.Remove(obj);
            _db.SaveChanges();
            List<Member> data = new List<Member>();
            data.Add(obj);
            return new ResponseViewModel<List<Member>>()
            {
                StatusCode = 200,
                Message = "Success Remove Data",
                Data = data
            };
        }
    }
}
